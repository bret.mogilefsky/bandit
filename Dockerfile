FROM python:latest
RUN pip install bandit==1.4.0
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
